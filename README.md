> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=412195)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# Webcolors

Hello!

This is a quick release of an include with all the
[webcolors](http://en.wikipedia.org/wiki/Web_colors#X11_color_names)
for Pawn.

All you have to do is download the content of `a_webcolors.inc` and add
this to your gamemode/filterscript:

```c
#include <a_webcolors>
```
Nothing more to say. Enjoy!

PS: Don't blame me for the indention, when you copy it to Pawno, it's
all straight.
